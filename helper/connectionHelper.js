const mongoose = require("mongoose");
const dbAccess = require("../config/dbAccess");

exports.connectDB = () => {
    mongoose.connect(
      dbAccess.prototype.connectURLToUsersDB,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err, res) => {
        if (err) throw err;
        else console.log("Conexión con BD Mongo exitosa!!")
      }
    ).catch(err => console.log(err));
}
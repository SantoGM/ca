let chai = require("chai");
let chaihttp = require("chai-http");
//incluir el app.js para que mocha pueda ejecutar las pruebas
let app = require("../app");
let should = chai.should();

chai.use(chaihttp);
chai.should();

var testUsername = "testUser" + new Date().getTime().toString()
var testPassword = "abcD123!"

describe("Signup test: ", function () { 
    it("should fail for empty user", (done) => {
        chai.request(app)
            .post("/api/signup/")
            .send({
                username: null,
                password: testPassword
            })
            .then((err, res) => {
                err.should.be.json
                
                chai.assert.equal(err.ok, false, "El OK no aparece como FALSE")
                chai.assert.equal(err.status, 400, "No devuelve error 400");
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });

    it("should fail due to invalid password", (done) => {
        chai.request(app)
            .post("/api/signup/")
            .send({
                username: testUsername,
                password: "badpass"
            })
            .then((err, res) => {
                err.should.be.json
                
                chai.assert.equal(err.ok, false, "El OK no aparece como FALSE")
                chai.assert.equal(err.status, 406, "No devuelve error 406");
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });

    it("should create a new user", (done) => {
        chai.request(app)
            .post("/api/signup/")
            .send({
                username: testUsername,
                password: testPassword
            })
            .then((res) => {
                res.should.be.json;

                chai.assert.equal(res.ok, true, "El OK no aparece como FALSE")
                chai.assert.equal(res.status, 200, "No devuelve status code 200");
                done()
            }).catch((err) =>{
                done(new Error(err.message))
            })          
    });

    it("should fail due to duplicated user", (done) => {
        chai.request(app)
            .post("/api/signup/")
            .send({
                username: testUsername,
                password: testPassword
            })
            .then((err, res) => {
                err.should.be.json
                
                chai.assert.equal(err.ok, false, "El OK no aparece como FALSE")
                chai.assert.equal(err.status, 409, "No devuelve error 409");
                done();             
            }).catch((err) =>{
                done(new Error(err.message))
            }) ;
    });
});

describe("Login test: ", function () {
    it("Should fail due to empty user", (done) => {
        chai.request(app)
        .post("/api/login")
        .send({
            username: null,
            password: testPassword
        })
        .then(err => {
            err.should.be.json

            chai.assert.equal(err.ok, false, "El OK no aparece como FALSE")
            chai.assert.equal(err.status, 401, "No devuelve error 401");
            done();  
        }).catch((err) =>{
            done(new Error(err.message))
        }) 
    })

    it("Should fail due to empty pass", (done) => {
        chai.request(app)
        .post("/api/login")
        .send({
            username: testUsername,
            password: null
        })
        .then(err => {
            err.should.be.json

            chai.assert.equal(err.ok, false, "El OK no aparece como FALSE")
            chai.assert.equal(err.status, 401, "No devuelve error 401");
            done();  
        }).catch((err) =>{
            done(new Error(err.message))
        }) 
    })
    
    it("Shoul log in correctly", (done) => {
        chai.request(app)
        .post("/api/login")
        .send({
            username: testUsername,
            password: testPassword
        })
        .then(res => {
            res.should.be.json

            chai.assert.equal(res.status, 200, "No devuelve error 200");
            done();  
        }).catch((err) =>{
            done(new Error(err.message))
        }) 
    })
})
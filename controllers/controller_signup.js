//DB Entities
const User = require("../models/user");

exports.signupGet = (req, res) => {
  //imprime los nombres de todos los usuarios registrados
  
  User.find({}, function(err, users) {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    var usernameList = []

    users.forEach(user => {
      usernameList.push(user.username)
    })

    res.json({
      ok: true,
      list: usernameList
    });
  });
}


exports.signupPost = function(req, res) {
    let body = req.body;

    let user = new User({
      username: body.username,
      password: body.password
    });

    let regExPw = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#$^+=!*()@%&]).{8,16}$")
    
    if(!regExPw.test(body.password)){
      let err = "El password debe incluir al menos una minúscula, una mayúscula, un número, un símbolo (#$^+=!*()@%&) y contener entre 8 y 16 caracteres"
      return res.status(406).json({
        ok: false,
        err
      });
    }

    user.save((err, userDB) => {
      if (err) {
        return res.status(400).json({
          ok: false,
          err
        });
      }
  
      res.json({
        ok: true,
        user: userDB
      });
    });
  }
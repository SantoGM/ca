var jwt = require("jsonwebtoken");

//DB Entities
const User = require("../models/user");

exports.loginPost = async (req, res) => {
    var bodyUsername = req.body.username
    var bodyPassword = req.body.password

    User.findOne({username : bodyUsername}, (err, user) =>{
        if (err) {
            return res.status(400).json({
              ok: false,
              err
            });
          }
        
        if((user !== null) && (user.password === bodyPassword)){
            var tokenData = { username: bodyUsername, ultimoLogin:new Date() };

            var token = jwt.sign(tokenData, process.env.CLAVEJWT, {
              expiresIn: 60 * 60 * 24 // expira en 24 horas
            });
        
            res.send({
              token
            });
        } else {
            res.status(401).send({
                error: "Usuario o contraseña inválidos"
            });
            return;
        }
    })
}
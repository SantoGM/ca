const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

mongoose.set('useCreateIndex', true)

let userSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: [true, 'Es obligatorio un UserName']
    },
    password:{
        type:String,
        required:[true,'Debe ingresar un password']
    }
});

userSchema.index({username: 'text'});
userSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = mongoose.model('User', userSchema);
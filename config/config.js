// ============================
//  Puerto
// ============================
process.env.PORT = process.env.PORT || 3000;

// ============================
//  Entorno
// ============================
process.env.NODE_ENV = process.env.NODE_ENV || "dev";
process.env.CLAVEJWT = process.env.CLAVEJWT || "lzdfjhvblELkLHBLFljlkEFLKJNDKLNEVILNSDlksdhfkKJHkhkHKLJhkFKJKJgkjHJFlyKU";

// ============================
//  Base de datos
// ============================
let urlDB;

if (process.env.NODE_ENV === "dev") {
  urlDB = "mongodb://localhost:27017/";
} else {
  urlDB = process.env.MONGO_URI;
}
process.env.URLDB = urlDB;
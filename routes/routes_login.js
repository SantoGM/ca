//*********BEGIN CONST DECLARATION**********/
//Server const
const express = require("express");
const app = express();
const controllerLogin = require('../controllers/controller_login')

//**********END CONST DECLARATION***********/

/**
 * @swagger
 * /api/login/:
 *  post:
 *      tags:
 *          - login
 *      parameters:
 *          - name: body
 *            in: body
 *            schema:
 *              properties:
 *                username:
 *                  type: string
 *                password:
 *                  type: string
 *            required:
 *              - username
 *              - password
 *      description: Si existe el usuario y el pass es correcto, entrega un JWT
 *      produces:
 *          - jasonwebtoken
 *      responses:
 *          200:
 *              description: Devuelve un JWT valido por 24hs
 *          400:
 *              description: Error de conexion a BD
 *          401:
 *              description: Usuario o Contrasena invalidos
 */
app.post("/", controllerLogin.loginPost)

module.exports = app
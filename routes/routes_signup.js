//Server const
const express = require("express");
const app = express();

//Middlewares
const { isUsernameTaken } = require("../middlewares/isUsernameTaken");

//Controllers
const controllerSignup = require('../controllers/controller_signup');

/**
 * @swagger
 * /api/signup/:
 *   get:
 *     tags:
 *       - signup
 *     description: Trae todos los usuarios registrados
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: devuelve json con la busqueda
 *       400:
 *         description: devuelve json avisando del error
 *
 */
app.get("/", controllerSignup.signupGet);

/**
 * @swagger
 * /api/signup/:
 *  post:
 *    tags:
 *      - signup
 *    description: Da de alta un usuario enviado por BODY.
 *    parameters:
 *      - name: body
 *        in: body
 *        schema:
 *          properties:
 *            username:
 *              type: string
 *            password:
 *              type: string
 *        required:
 *          - username
 *          - password
 *    produces:
 *      - applicatioon/json
 *    responses:
 *      200:
 *        descrpition: Devuelve JSon con el nuevo usuario
 *      400:
 *        description: JSon con error de conexión o guardado a BD
 *      409:
 *        description: Error si intento dar de alta usuario existente
 */
app.post("/", isUsernameTaken, controllerSignup.signupPost);

module.exports = app;
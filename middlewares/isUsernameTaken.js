exports.isUsernameTaken = async function(req, res, next){
    var bodyUsername = req.body.username;
    var mongoose = require("mongoose")
    var dbAccess = require("../config/dbAccess");
    var User = require("../models/user");

    await mongoose.connect(
        dbAccess.prototype.connectURLToUsersDB,
        { useNewUrlParser: true, useUnifiedTopology: true },
        (err, res) => {
          if (err) {
            return res.status(400).json({
              ok: false,
              err
            });
          }
        }
    ).catch(err => console.log(err));

    await User.find({ username: bodyUsername }, (err, user) => {
        
        if (err) {
            return res.status(400).json({
              ok: false,
              err
            });
          }

        if (user.length > 0) {
            return res
                .status(409)
                .json({ error: "Ya existe el usuario" });
        }
        next()
    })
}